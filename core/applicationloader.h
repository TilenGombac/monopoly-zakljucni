#ifndef APPLICATIONLOADER_H
#define APPLICATIONLOADER_H

class MainWindow;

class ApplicationLoader
{
public:
	static void load(MainWindow *window);

};

#endif // APPLICATIONLOADER_H
