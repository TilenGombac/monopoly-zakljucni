#ifndef PLAYERDISPLAYUTILS_H
#define PLAYERDISPLAYUTILS_H

#include <QRect>

class PlayerDisplayUtils
{
public:
	static QRect getGeometry(const int &numberOfPlayers);
};

#endif // PLAYERDISPLAYUTILS_H
