#ifndef SIDEBARUTILS_H
#define SIDEBARUTILS_H

#include <QRect>

class SidebarUtils
{
public:
	static QRect getGeometry();
};

#endif // SIDEBARUTILS_H
