#ifndef SIDEBARDISPLAYUTILS_H
#define SIDEBARDISPLAYUTILS_H

class QPainter;

class SidebarDisplayUtils
{
public:
	static void drawApplicationName(QPainter *painter);
	static void drawBorders(QPainter *painter);

};

#endif // SIDEBARDISPLAYUTILS_H
