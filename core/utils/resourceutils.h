#ifndef RESOURCEUTILS_H
#define RESOURCEUTILS_H

#include <QString>

class ResourceUtils
{
public:
	static QString getStyleSheet(QString path);
};

#endif // RESOURCEUTILS_H
