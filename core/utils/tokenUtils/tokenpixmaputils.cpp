#include "tokenpixmaputils.h"

QPixmap TokenPixmapUtils::getAutomobilePixmap()
{
	return QPixmap(":/img/img/tokens/AutomobileToken.png");
}

QPixmap TokenPixmapUtils::getBattleshipPixmap()
{
	return QPixmap(":/img/img/tokens/BattleshipToken.png");
}

QPixmap TokenPixmapUtils::getHowitzerPixmap()
{
	return QPixmap(":/img/img/tokens/HowitzerToken.png");
}

QPixmap TokenPixmapUtils::getScottishTerrierPixmap()
{
	return QPixmap(":/img/img/tokens/ScottishTerrierToken.png");
}

QPixmap TokenPixmapUtils::getThimblePixmap()
{
	return QPixmap(":/img/img/tokens/ThimbleToken.png");
}

QPixmap TokenPixmapUtils::getTopHatPixmap()
{
	return QPixmap(":/img/img/tokens/TopHatToken.png");
}
