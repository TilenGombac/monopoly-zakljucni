#ifndef TOKENPIXMAPUTILS_H
#define TOKENPIXMAPUTILS_H

#include <QPixmap>

class TokenPixmapUtils
{
public:
	static QPixmap getAutomobilePixmap();
	static QPixmap getBattleshipPixmap();
	static QPixmap getHowitzerPixmap();
	static QPixmap getScottishTerrierPixmap();
	static QPixmap getThimblePixmap();
	static QPixmap getTopHatPixmap();

};

#endif // TOKENPIXMAPUTILS_H
