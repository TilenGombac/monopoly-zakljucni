#ifndef CONSTANTUTILS_H
#define CONSTANTUTILS_H

class QSize;
class QWidget;

class ConstantUtils
{
public:
	// Size constants

	static int FIELD_WIDTH;
	static int FIELD_HEIGHT;

	static int SIDEBAR_WIDTH;
};

#endif // CONSTANTUTILS_H
