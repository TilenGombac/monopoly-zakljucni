#ifndef FIELDTRANSLATEUTILS_H
#define FIELDTRANSLATEUTILS_H

#include <QPoint>

class FieldTranslateUtils
{
public:
	static QPoint getTranslation(const int &fieldId);
};

#endif // FIELDTRANSLATEUTILS_H
