#ifndef FIELDROTATIONUTILS_H
#define FIELDROTATIONUTILS_H

class FieldRotationUtils
{
public:
	static int getRotation(const int &fieldId);
};

#endif // FIELDROTATIONUTILS_H
