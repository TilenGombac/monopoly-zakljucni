#ifndef BUILDINGUTILS_H
#define BUILDINGUTILS_H

class Field;

class BuildingUtils
{
public:
	static float getBuildingPrice(Field *field);

};

#endif // BUILDINGUTILS_H
