#ifndef WINDOWDISPLAYUTILS_H
#define WINDOWDISPLAYUTILS_H

class QColor;
class QPainter;

class WindowDisplayUtils
{
public:
	static void drawBackground(QPainter *painter, const QColor &color);

};

#endif // WINDOWDISPLAYUTILS_H
